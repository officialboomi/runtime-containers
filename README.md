# Runtime Containers
This repository contains all Runtime containerization reference architectures, guidance, and definitions.
## Contents

[Amazon Elastic Kubernetes Service - Boomi Molecule, Boomi Gateway & Boomi Atom Cloud](https://bitbucket.org/officialboomi/runtime-containers/src/master/eks/)

[Kubernetes Reference Architecture - Boomi Molecule, Boomi Gateway & Boomi Atom Cloud](https://bitbucket.org/officialboomi/runtime-containers/src/master/Kubernetes/)

[Docker Compose Reference Architecture - Boomi Molecule, Boomi Gateway & Boomi Atom Cloud & Boomi Atom](https://bitbucket.org/officialboomi/runtime-containers/src/master/docker-compose/)


