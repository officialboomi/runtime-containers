Contents:

* [Intent](#markdown-header-intent)
* [Known Limitations](#markdown-header-known-limitations)
* [Requirements](#markdown-header-requirements)
* [Configuration Review & Deployment](#markdown-header-configuration-review-deployment)

--------------------------------------------------------------------------------------

# Docker Compose Reference Architecture - Boomi Molecule, Boomi Atom Cloud, Boomi Atom, Boomi Gateway

## Intent
The following reference architecture outlines the requirements and limitations associated with running a Boomi Atom, Cloud, Gateway or Molecule in a Docker-based environment.

Note that the Boomi Private Atom Cloud is similar in nature to the Boomi Molecule. This reference document can also be used to run a Cloud Molecule within a Boomi Private Atom Cloud with minor changes. These changes are detailed within this document.

**It is expected that the user has a working knowledge of docker concepts and components.**

Users can apply the Docker technical foundation detailed in the following document to their own particular use cases and Docker configurations.

--------------------------------------------------------------------------------------

## Known Limitations
* If you receive an error about access to the mounted path while executing the docker compose file, ensure that the mounted host directory has been given read, write, and execute permissions to UID 1000. Then, add the environment variable `VALIDATE_MOUNT_ACCESS` to `environment` field in the docker compose file and set to `false`.
* The SAP connector and the SAP JCo V2 connector are currently not compatible with the version 4 images.
--------------------------------------------------------------------------------------

## Requirements

#### Boomi Molecule/Cloud/Atom/Gateway Configuration / Infrastructure
The following list outlines the general requirements that are needed to deploy a Boomi Molecule, Boomi Gateway or Boomi Atom Cloud.

* Boomi account credentials.
* NFS solution provisioned and reachable by the docker environment.
    * The Boomi Molecule/Cloud/Gateway requires the provisioning and availability of a Network File System (NFS).
* A Boomi Docker Image. This reference architecture currently supports:
    * Boomi Molecule Docker image ([publicly available](https://hub.docker.com/r/boomi/molecule)).
    * Boomi Atom Cloud Docker image ([publicly available](https://hub.docker.com/r/boomi/cloud)).
    * Boomi Atom Docker image ([publicly available](https://hub.docker.com/r/boomi/atom)).
    * Boomi Gateway Docker image ([publicly available](https://hub.docker.com/r/boomi/gateway))


--------------------------------------------------------------------------------------
## Configuration Review & Deployment
The following configuration compose files install a Docker container for an Atom, Molecule, Gateway or Cloud:

* boomi_atom_compose.yaml
* boomi_molecule_compose.yaml
* boomi_cloud_compose.yaml
* boomi_gateway_compose.yaml

#### Installing an Atom: 
* Update the boomi_atom_compose.yml with `image` version(to any Version 4 image), `container_name`,`hostDirectory` listed under volumes. 
* Environment variables listed in the compose file are all mandatory fields for a basic Atom installation, to learn more about other optional environment variables V4 image supports please refer [Boomi Atom](https://hub.docker.com/r/boomi/atom) 
and include them under `environment`. 
* After modifying the compose file, run `docker-compose -f  boomi_atom_compose.yml up`.

#### Installing a multi node Molecule or Cloud or Gateway:
* Follow the same steps provided for Atom installation and update the compose file accordingly.
* Make sure `BOOMI_ATOMNAME` and `hostDirectory` are common for all the nodes. 
* Any common environment variables across all the nodes can be placed under yaml anchor `&env`. For additional information refer [Boomi Cloud](https://hub.docker.com/r/boomi/cloud) , [Boomi Molecule](https://hub.docker.com/r/boomi/molecule) and  [Boomi Gateway](https://hub.docker.com/r/boomi/gateway)
* After modifying the docker compose file, run `docker-compose -f boomi_molecule_compose.yml up` for Molecule installation , `docker-compose -f boomi_cloud_compose.yml up` for Cloud installation and `docker-compose -f boomi_gateway_compose.yml up` for Gateway installation.
* **Note:** If the first, or head, node installation takes long to finish and the other nodes can't start due to the failure of the health check, try increasing the number of retries or duration of the interval, or manually restart the Docker containers of the affected nodes.